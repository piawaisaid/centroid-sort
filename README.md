# Kelompok A15

Repository ini dibuat sebagai penunjang untuk tugas paper DAA kelompok kami yang berjudul **Analisis Algoritma Centroid Sort: Penerapan Teknik Clustering untuk Mempersingkat Running Time Algoritma Sorting**.

Main class dari project ini terdapat di MainClass.java

## Group Member
- Francois Ferdinand Dahny Ginting (2006596245)
- Muhammad Afiful Amin (1706039963)
- Piawai Said Umbara (1806235933)