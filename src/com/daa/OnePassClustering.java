package src.com.daa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class OnePassClustering {

    public static int[][] clusterize(int[] arr) {
        shuffleArray(arr);
        int[] centroids = Arrays.copyOfRange(arr, 0, 2);
        
        int min, max;
        if (centroids[0] < centroids[1]) {
            min = centroids[0];
            max = centroids[1];
        } else {
            min = centroids[1];
            max = centroids[0];
        }
        
        List<Integer> lst1 = new ArrayList<>();
        List<Integer> lst2 = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            if (Math.pow(arr[i] - min, 2) < Math.pow(arr[i] - max, 2)) {
                lst1.add(arr[i]);
            } else {
                lst2.add(arr[i]);
            }
        }

        int[] partition1 = new int[lst1.size()];
        for (int i = 0; i < lst1.size(); i++) {
            partition1[i] = lst1.get(i);
        }

        int[] partition2 = new int[lst2.size()];
        for (int i = 0; i < lst2.size(); i++) {
            partition2[i] = lst2.get(i);
        }

        return (new int[][] { partition1, partition2 });
    }

    private static void shuffleArray(int[] arr) {
        Random rand = ThreadLocalRandom.current();
        for (int i = arr.length - 1; i > 0; i--) {
            int randomIndextoSwap = rand.nextInt(i + 1);
            swap(arr, randomIndextoSwap, i);
        }
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
