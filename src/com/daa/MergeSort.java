package src.com.daa;

public class MergeSort implements SortingAlgo {

    public void sort(int[] arr, int from, int to) {
        if (from < to) {
            int middle = (from + to) / 2;
            sort(arr, from, middle);
            sort(arr, middle + 1, to);
            merge(arr, from, middle, to);
        }
    }

    private static void merge(int[] arr, int from, int middle, int to) {
        int[] left = new int[middle - from + 2];
        int[] right = new int[to - middle + 1];

        System.arraycopy(arr, from, left, 0, left.length - 1);
        System.arraycopy(arr, middle + 1, right, 0, right.length - 1);
        left[left.length - 1] = Integer.MAX_VALUE;
        right[right.length - 1] = Integer.MAX_VALUE;

        int left_pointer = 0;
        int right_pointer = 0;
        for (int arr_pointer = from; arr_pointer <= to; arr_pointer++) {
            if (left[left_pointer] <= right[right_pointer]) {
                arr[arr_pointer] = left[left_pointer++];
            } else {
                arr[arr_pointer] = right[right_pointer++];
            }
        }
    }
}