package src.com.daa;

public class MainClass {

    public static void main(String[] args) {

        // Memilih sorting algorithm yang akan digunakan
        SortingAlgo quick = new QuickSort();
        CentroidSort centroid = new CentroidSort();

        // Generate dataset
        int[] arr1 = RandomNumbersGenerator.generateReversed(52500);
        int[] arr2 = new int[arr1.length];
        System.arraycopy(arr1, 0, arr2, 0, arr1.length);

        // Mencetak waktu eksekusi sorting algorithm yang dipilih
        long startTime1 = System.nanoTime();
        centroid.sort(arr1, quick);
        long elapsedTime1 = System.nanoTime() - startTime1;
        System.out.println("Total execution time: " + elapsedTime1);

        // Mencetak waktu eksekusi algoritma centroid sort
        long startTime2 = System.nanoTime();
        quick.sort(arr2, 0, arr2.length - 1);
        long elapsedTime2 = System.nanoTime() - startTime2;
        System.out.println("Total execution time: " + elapsedTime2);
    }
}
