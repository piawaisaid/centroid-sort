package src.com.daa;

public class CentroidSort {
    
    public void sort(int[] arr, SortingAlgo sortingAlgo) {
        int[][] partitions = OnePassClustering.clusterize(arr);
        sortingAlgo.sort(partitions[0], 0, partitions[0].length - 1);
        sortingAlgo.sort(partitions[1], 0, partitions[1].length - 1);

        for (int i = 0; i < partitions[0].length; i++) {
            arr[i] = partitions[0][i];
        }
        for (int i = 0; i < partitions[1].length; i++) {
            arr[i + partitions[0].length] = partitions[1][i];
        }
    }
}
