package src.com.daa;

import java.util.Random;

public class RandomNumbersGenerator {

    /*
     * Fungsi untuk men-generate array yang berisikan kumpulan integer acak.
     * 
     * @param n     jumlah integer yang ingin de-generate
     * @return      array yang berisi kumpulan integer acak
    */
    public static int[] generate(int n) {
        Random rd = new Random();
        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rd.nextInt();
        }
        return arr;
    }

    /*
     * Fungsi untuk men-generate array yang berisikan kumpulan integer yang bernilai sama.
     * 
     * @param x     nilai integer yang ingin di-generate
     * @param n     jumlah integer yang ingin de-generate
     * @return      array yang berisi kumpulan integer yang bernilai sama
    */
    public static int[] generateEquals(int x, int n) {
        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = x;
        }
        return arr;
    }

    /*
     * Fungsi untuk men-generate array yang berisikan kumpulan integer yang terurut naik.
     * 
     * @param n     jumlah integer yang ingin de-generate
     * @return      array yang berisi kumpulan integer yang terurut naik
    */
    public static int[] generateSorted(int n) {
        int[] arr = generate(n);
        MergeSort merge = new MergeSort();
        merge.sort(arr, 0, arr.length - 1);
        return arr;
    }

    /*
     * Fungsi untuk men-generate array yang berisikan kumpulan bilangan yang terurut menurun.
     * 
     * @param n     jumlah integer yang ingin de-generate
     * @return      array yang berisi kumpulan integer yang terurut menurun
    */
    public static int[] generateReversed(int n) {
        int[] arr = generate(n);
        MergeSort merge = new MergeSort();
        merge.sort(arr, 0, arr.length - 1);
        
        int[] reversed = new int[arr.length];
        for (int i = 0; i < reversed.length; i++) {
            reversed[i] = arr[(arr.length - 1) - i];
        }

        return reversed;
    }
}
