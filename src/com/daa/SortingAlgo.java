package src.com.daa;

public interface SortingAlgo {
    void sort(int[] arr, int from, int to);
}
