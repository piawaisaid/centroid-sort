package src.com.daa;

public class QuickSort implements SortingAlgo {

    public void sort(int[] arr, int from, int to) {
        if (from < to) {
            int partitionIndex = partition(arr, from, to);
            sort(arr, from, partitionIndex - 1);
            sort(arr, partitionIndex + 1, to);
        }
    }
    
    private static int partition(int[] arr, int from, int to) {
        int pivot = arr[to];
        int lastSmallerEl = from - 1;
        for (int curr = from; curr < to; curr++) {
            if (arr[curr] <= pivot) {
                lastSmallerEl++;
                swap(arr, lastSmallerEl, curr);
            }
        }
        swap(arr, lastSmallerEl + 1, to);
        return (lastSmallerEl + 1);
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
